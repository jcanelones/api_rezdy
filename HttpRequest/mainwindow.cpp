#include "mainwindow.h"
#include "ui_mainwindow.h"

QString selector;
QString apiKey = "?apiKey=clave_usuario";
QString url_rezdy = "https://api.rezdy.com/v1/";
QUrl URL;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->comboBox->addItem("Seleccione");
    ui->comboBox->addItem("xml");
    ui->comboBox->addItem("json");
    ui->ButtonSave->setEnabled(false);
    manager = new QNetworkAccessManager(this);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_ButtonGet_clicked(){

    URL = url_rezdy + "customers" + apiKey; ////// Aqui agregamos lo que necesitamos descargar
    QNetworkRequest requerimiento(URL);
    if(selector=="xml"){
        requerimiento.setRawHeader("Accept", "application/xml");
        reply = manager->get(requerimiento);
    }
    else if(selector=="json"){
        requerimiento.setRawHeader("Accept", "application/json");
        reply = manager->get(requerimiento);
    }
    else {
        ui->ButtonSave->setEnabled(false);
        ui->plainTextEdit->clear();
        QMessageBox::information(this,"Información","Debe seleccionar el tipo de formato");
        return;
    }
    connect(reply,SIGNAL(finished()),this,SLOT(printData()));;
}

void MainWindow::on_ButtonSave_clicked(){
    QString file = QFileDialog::getSaveFileName(this,"Save File", "datos."+selector);
    QFile datafile(file);
    if(datafile.open(QFile::WriteOnly | QFile::Text)){
        QTextStream stream(&datafile);
        stream << ui->plainTextEdit->toPlainText();
        datafile.flush();
        datafile.close();
    }
}

void MainWindow::on_comboBox_activated(const QString &arg1){
    selector = ui->comboBox->currentText();
    ui->ButtonSave->setEnabled(false);
    ui->plainTextEdit->clear();
}

void MainWindow::printData()
{
    ui->plainTextEdit->setPlainText(reply->readAll());
    ui->ButtonSave->setEnabled(true);
}





